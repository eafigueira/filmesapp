package com.textoit.filmeApp.controller;

import com.textoit.filmeApp.domain.Filme;
import com.textoit.filmeApp.dto.FilmeDto;
import com.textoit.filmeApp.service.FilmeService;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author emerson
 */
@RestController
@RequestMapping("/filme")
public class FilmeController {

    @Autowired
    private FilmeService filmeService;

    @PostMapping
    ResponseEntity<Filme> inserirFilme(@Valid @RequestBody FilmeDto filme) {
        return ResponseEntity.ok(filmeService.inserirFilme(filme));
    }

    @GetMapping
    ResponseEntity<List<Filme>> listarFilmes() {
        return ResponseEntity.ok(filmeService.listarFilmes());
    }

    @DeleteMapping("{id:[0-9]+}")
    ResponseEntity<Filme> excluirFilme(@PathVariable Long id) {
        Optional<Filme> filme = filmeService.excluirFilme(id);
        if (!filme.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(filme.get());
    }
}
