package com.textoit.filmeApp.controller;

import com.textoit.filmeApp.dto.ProducerAggregation;
import com.textoit.filmeApp.dto.ProducerAggregationConsolidated;
import com.textoit.filmeApp.dto.ProducerSummary;
import com.textoit.filmeApp.service.ProducerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author emerson
 */
@RestController
@RequestMapping("/produtor")
public class ProducerController {

    private final ProducerService producerService;

    @Autowired
    public ProducerController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @GetMapping("/sumario")
    public ResponseEntity<ProducerAggregationConsolidated> getSumario() {
        ProducerAggregationConsolidated producerAwardsAggregationConsolidated = producerService.getSummaryProducerAwards();
        return ResponseEntity.ok(producerAwardsAggregationConsolidated);
    }

}
