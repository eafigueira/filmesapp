package com.textoit.filmeApp.repository;

import com.textoit.filmeApp.domain.Studio;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emerson
 */
@Repository
public interface StudioRepository extends JpaRepository<Studio, Long> {

    Optional<Studio> findByName(String name);
}
