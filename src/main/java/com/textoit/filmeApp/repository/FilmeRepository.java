package com.textoit.filmeApp.repository;

import com.textoit.filmeApp.domain.Filme;
import com.textoit.filmeApp.domain.Producer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emerson
 */
@Repository
public interface FilmeRepository extends JpaRepository<Filme, Long> {
    List<Filme> findByProducersAndWinnerTrue(Producer producer);
}
