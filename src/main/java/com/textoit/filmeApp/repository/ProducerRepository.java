package com.textoit.filmeApp.repository;

import com.textoit.filmeApp.domain.Producer;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emerson
 */
@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long> {

    Optional<Producer> findByName(String name);

    @Query(value = "SELECT producer.id, producer.name "
            + "from filme, producer_filme, producer "
            + "WHERE producer_filme.producer_id = producer.id "
            + "AND producer_filme.filme_id = filme.id "
            + "AND filme.winner = TRUE "
            + "GROUP BY producer.id, producer.name "
            + "HAVING count(*) > 1", nativeQuery = true)
    List<Producer> producersWithTwoAwards();
}
