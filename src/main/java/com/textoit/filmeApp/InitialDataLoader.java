package com.textoit.filmeApp;

import com.textoit.filmeApp.dto.FilmeDto;
import com.textoit.filmeApp.service.FilmeService;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 *
 * @author emerson
 */
@Component
@Profile("!test")
public class InitialDataLoader implements CommandLineRunner {

    private final FilmeService filmeService;

    @Autowired
    public InitialDataLoader(FilmeService filmeService) {
        this.filmeService = filmeService;
    }

    @Override
    public void run(String... args) throws Exception {
        Path path = Paths.get(URI.create(getClass().getResource("/movielist.csv").toString()));
        List<String> rawSource = Files.readAllLines(path).stream().collect(Collectors.toList());
        IntStream.range(1, rawSource.size()).forEach(i -> saveFilme(rawSource.get(i)));
    }

    private void saveFilme(String line) {
        List<String> values = Collections.list(new StringTokenizer(line, ";")).stream()
                .map(Object::toString)
                .map(String::trim)
                .collect(Collectors.toList());

        Integer year = Integer.valueOf(values.get(0));
        String title = values.get(1);
        List<String> studios = Arrays.asList(values.get(2).split(","));
        List<String> producers = Arrays.asList(values.get(3).split(","));
        Boolean winner = values.size() == 5 ? values.get(4).equals("yes") : false;

        FilmeDto filme = new FilmeDto();
        filme.setYear(year);
        filme.setTitle(title);
        filme.setStudios(studios);
        filme.setProducers(producers);
        filme.setWinner(winner);

        filmeService.inserirFilme(filme);
    }
}