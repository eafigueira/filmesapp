package com.textoit.filmeApp.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author emerson
 */
@Data
@AllArgsConstructor
public class ProducerAggregationConsolidated {

    List<ProducerAggregation> min;
    List<ProducerAggregation> max;
    
}
