package com.textoit.filmeApp.dto;

import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * @author emerson
 */
@Getter
@Setter
public class FilmeDto {
    @NotNull
    private Integer year;
    @NotNull
    @NotBlank
    private String title;
    @NotEmpty
    private List<String> studios;
    @NotEmpty
    private List<String> producers;
    private Boolean winner;

}
