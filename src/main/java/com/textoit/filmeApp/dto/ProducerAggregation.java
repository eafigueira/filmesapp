package com.textoit.filmeApp.dto;

import lombok.Data;

/**
 *
 * @author emerson
 */
@Data
public class ProducerAggregation {

    private String producer;
    private Integer interval;
    private Integer previousWin;
    private Integer followingWin;
}
