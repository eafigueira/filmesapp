package com.textoit.filmeApp.service;

import com.textoit.filmeApp.domain.Filme;
import com.textoit.filmeApp.domain.Producer;
import com.textoit.filmeApp.dto.ProducerAggregation;
import com.textoit.filmeApp.dto.ProducerAggregationConsolidated;
import com.textoit.filmeApp.repository.FilmeRepository;
import com.textoit.filmeApp.repository.ProducerRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author emerson
 */
@Service
public class ProducerService {

    private final ProducerRepository producerRepository;
    private final FilmeRepository filmeRepository;

    @Autowired
    public ProducerService(ProducerRepository producerRepository, FilmeRepository filmeRepository) {
        this.producerRepository = producerRepository;
        this.filmeRepository = filmeRepository;
    }

    public List<ProducerAggregation> getProducerAggregation(Producer producer) {
        List<Filme> list = filmeRepository.findByProducersAndWinnerTrue(producer)
                .stream()
                .sorted(Comparator.comparingInt(Filme::getYear))
                .collect(Collectors.toList());
        return calculeProducerAggregation(producer, list);
    }

    private List<ProducerAggregation> calculeProducerAggregation(Producer producer, List<Filme> data) {
        List<ProducerAggregation> producerList = new ArrayList<>();
        StreamEx<Filme> spx = StreamEx.of(data);
        spx.forPairs((current, next) -> {
            ProducerAggregation producerAggregation = new ProducerAggregation();
            producerAggregation.setProducer(producer.getName());
            producerAggregation.setFollowingWin(next.getYear());
            producerAggregation.setPreviousWin(current.getYear());
            producerAggregation.setInterval(next.getYear() - current.getYear());
            producerList.add(producerAggregation);
        });
        return producerList;
    }

    public List<ProducerAggregation> listProducerAggregationsWithTwoAwards() {
        List<ProducerAggregation> result = new ArrayList<>();
        List<Producer> producersWithTwoAwards = producerRepository.producersWithTwoAwards();
        producersWithTwoAwards.forEach(producer -> {
            List<ProducerAggregation> producerAggregation = getProducerAggregation(producer);
            producerAggregation.forEach(ag -> result.add(ag));
        });
        return result;
    }

    public ProducerAggregationConsolidated getSummaryProducerAwards() {
        List<ProducerAggregation> listProducerAggregationsWithTwoAwards = listProducerAggregationsWithTwoAwards();
        Comparator<ProducerAggregation> comparator = Comparator.comparing(ProducerAggregation::getInterval);
        ProducerAggregation min = listProducerAggregationsWithTwoAwards.stream().min(comparator).get();
        ProducerAggregation max = listProducerAggregationsWithTwoAwards.stream().max(comparator).get();
        List<ProducerAggregation> minList = listProducerAggregationsWithTwoAwards.stream().filter(ag -> Objects.equals(ag.getInterval(), min.getInterval())).collect(Collectors.toList());
        List<ProducerAggregation> maxList = listProducerAggregationsWithTwoAwards.stream().filter(ag -> Objects.equals(ag.getInterval(), max.getInterval())).collect(Collectors.toList());
        return new ProducerAggregationConsolidated(minList, maxList);
    }

}
