package com.textoit.filmeApp.service;

import com.textoit.filmeApp.domain.Filme;
import com.textoit.filmeApp.domain.Producer;
import com.textoit.filmeApp.domain.Studio;
import com.textoit.filmeApp.dto.FilmeDto;
import com.textoit.filmeApp.repository.FilmeRepository;
import com.textoit.filmeApp.repository.ProducerRepository;
import com.textoit.filmeApp.repository.StudioRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author emerson
 */
@Service
public class FilmeService {

    private final FilmeRepository filmeRepository;
    private final StudioRepository studioRepository;
    private final ProducerRepository producerRepository;

    @Autowired
    public FilmeService(FilmeRepository filmeRepository, StudioRepository studioRepository, ProducerRepository producerRepository) {
        this.filmeRepository = filmeRepository;
        this.studioRepository = studioRepository;
        this.producerRepository = producerRepository;
    }

    private Set<Studio> saveStudios(List<String> studios) {
        Set<Studio> result = new HashSet<>();
        List<String> trimedStudios = studios.stream().map(name -> name.trim()).collect(Collectors.toList());
        trimedStudios.forEach(name -> {
            Studio studio = studioRepository.findByName(name).orElse(new Studio(name));
            result.add(studioRepository.saveAndFlush(studio));
        });
        return result;
    }

    private Set<Producer> saveProducers(List<String> producers) {
        Set<Producer> result = new HashSet<>();
        List<String> trimedProducers = producers.stream().map(name -> name.trim()).collect(Collectors.toList());
        trimedProducers.forEach(name -> {
            Producer producer = producerRepository.findByName(name).orElse(new Producer(name));
            result.add(producerRepository.saveAndFlush(producer));
        });
        return result;
    }

    public Optional<Filme> findById(Long id) {
        return filmeRepository.findById(id);
    }

    public Filme inserirFilme(FilmeDto filme) {
        Filme filmeToSave = new Filme();
        filmeToSave.setProducers(saveProducers(filme.getProducers()));
        filmeToSave.setStudios(saveStudios(filme.getStudios()));
        filmeToSave.setTitle(filme.getTitle());
        filmeToSave.setWinner(filme.getWinner());
        filmeToSave.setYear(filme.getYear());

        return filmeRepository.save(filmeToSave);
    }

    public Optional<Filme> excluirFilme(Long id) {
        Optional<Filme> filme = filmeRepository.findById(id);
        if (filme.isPresent()) {
            filmeRepository.deleteById(id);
        }
        return filme;
    }

    public List<Filme> listarFilmes() {
        return filmeRepository.findAll(Sort.by(Sort.Direction.DESC, "year", "title"));
    }
}
