package com.textoit.filmeApp;

import com.textoit.filmeApp.dto.FilmeDto;
import com.textoit.filmeApp.service.FilmeService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 *
 * @author emerson
 */
@Component
@Profile("test")
public class InitialDataLoaderTest implements CommandLineRunner {

    private final FilmeService filmeService;

    @Autowired
    public InitialDataLoaderTest(FilmeService filmeService) {
        this.filmeService = filmeService;
    }

    @Override
    public void run(String... args) throws Exception {
        FilmeDto filme = new FilmeDto();
        filme.setYear(2020);
        filme.setTitle("FILME DE TESTE");
        filme.setStudios(Lists.newArrayList("STUDIOS DE TESTE"));
        filme.setProducers(Lists.newArrayList("PRODUTOR DE TESTE"));
        filme.setWinner(true);

        filmeService.inserirFilme(filme);

        filme = new FilmeDto();
        filme.setYear(2020);
        filme.setTitle("FILME DE TESTE 2");
        filme.setStudios(Lists.newArrayList("STUDIOS DE TESTE"));
        filme.setProducers(Lists.newArrayList("PRODUTOR DE TESTE"));
        filme.setWinner(true);

    }

}
