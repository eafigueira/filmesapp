package com.textoit.filmeApp;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.textoit.filmeApp.dto.FilmeDto;
import com.textoit.filmeApp.service.FilmeService;
import net.bytebuddy.matcher.ElementMatchers;
import org.assertj.core.util.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author emerson
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class FilmeControllerTest {

    MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

    @Test
    public void testListFilme() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/filme"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void createFilme() throws Exception {

        FilmeDto dto = new FilmeDto();
        dto.setProducers(Lists.newArrayList("PRODUTOR DE TESTE"));
        dto.setStudios(Lists.newArrayList("STUDIO DE TESTE"));
        dto.setTitle("FILME PARA TESTE");
        dto.setWinner(Boolean.FALSE);
        dto.setYear(2020);

        JsonMapper mapper = new JsonMapper();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(MockMvcRequestBuilders.post("/filme")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void createFilmeWithoutTitle() throws Exception {

        FilmeDto dto = new FilmeDto();
        dto.setProducers(Lists.newArrayList("PRODUTOR DE TESTE"));
        dto.setStudios(Lists.newArrayList("STUDIO DE TESTE"));
        dto.setWinner(Boolean.FALSE);
        dto.setYear(2020);

        JsonMapper mapper = new JsonMapper();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(MockMvcRequestBuilders.post("/filme")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void createFilmeWithoutProducers() throws Exception {
        FilmeDto dto = new FilmeDto();
        dto.setTitle("FILME PARA TESTE");
        dto.setStudios(Lists.newArrayList("STUDIO DE TESTE"));
        dto.setWinner(Boolean.FALSE);
        dto.setYear(2020);

        JsonMapper mapper = new JsonMapper();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(MockMvcRequestBuilders.post("/filme")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void createFilmeWithoutStudio() throws Exception {
        FilmeDto dto = new FilmeDto();
        dto.setTitle("FILME PARA TESTE");
        dto.setProducers(Lists.newArrayList("PRODUTOR DE TESTE"));
        dto.setWinner(Boolean.FALSE);
        dto.setYear(2020);

        JsonMapper mapper = new JsonMapper();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(MockMvcRequestBuilders.post("/filme")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteFilme() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/filme/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(2)));

    }

    @Test
    public void deleteFilmeNotExistID() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/filme/5678567856785"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

}
