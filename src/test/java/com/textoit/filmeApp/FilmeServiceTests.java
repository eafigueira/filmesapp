package com.textoit.filmeApp;

import com.textoit.filmeApp.domain.Filme;
import com.textoit.filmeApp.dto.FilmeDto;
import com.textoit.filmeApp.service.FilmeService;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
class FilmeServiceTests {

    private final FilmeService filmeService;

    @Autowired
    public FilmeServiceTests(FilmeService filmeService) {
        this.filmeService = filmeService;
    }

    @Test
    public void testCreateFilme() {
        FilmeDto filmeInput = new FilmeDto();
        filmeInput.setYear(2020);
        filmeInput.setWinner(Boolean.TRUE);
        filmeInput.setTitle("FILME DE TESTE 3");
        filmeInput.setStudios(Lists.newArrayList("EMERSON STUDIOS"));
        filmeInput.setProducers(Lists.newArrayList("EMERSON FIGUEIRA"));
        Filme filme = filmeService.inserirFilme(filmeInput);
        Assertions.assertThat(filme.getTitle()).isEqualTo(filmeInput.getTitle());
    }

    @Test
    public void testDeleteFilme() {
        Optional<Filme> foundFilme = filmeService.findById(1L);
        Assertions.assertThat(foundFilme).isPresent().containsInstanceOf(Filme.class);
        Assertions.assertThat(foundFilme.get().getId().equals(1L));
        Optional<Filme> fileExcluido = filmeService.excluirFilme(1L);
        Assertions.assertThat(fileExcluido).isPresent().containsInstanceOf(Filme.class);
        Assertions.assertThat(fileExcluido.get().getId().equals(1L));
    }

}
